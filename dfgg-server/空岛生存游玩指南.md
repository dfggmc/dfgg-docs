# 常用命令:
/skyblock help - 显示命令列表  
/skyblock language chinese.ini - 将语言设置为中文  
/skyblock join - 加入 skyblock 世界  
/skyblock play - 进入空岛并开始建造  
/skyblock recreate - 从原理图文件重新创建生成  
/challenges - 列出所有挑战  
/challenges info <name> - 显示挑战的信息  
/challenges complete <name> - 完成挑战  
/challenges check <name> <req,rew> [rpt] - 获取项目并检查它们是否正确（rpt＝可重复）  
/island home - 传送回你的家所在的空岛  
/island home set - 更改空岛上的家庭位置  
/island obsidian - 将黑石背景改为熔岩  
/island add <player> - 将玩家添加到好友列表  
/island remove <player> - 将玩家从好友列表中删除  
/island join <player> - 传送到朋友空岛  
/island list - 列出您可以加入的朋友的空岛  
/island restart - 启动一个新的空岛  
# 注意事项:
死亡掉落，注意不要掉下去：)  
一般来说初始物资会在你的背包中  